# Deploy your Vue app to Heroku

Before you start, ensure you have installed the Heroku CLI.

#### Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)


Follow the steps to setup your Vue app for deployment:

> **NB:** This must be done in the **root** of your project directory 
> The same place you find the `package.json` file

1. Create a `static.json` file:

```json
{
  "root": "dist",
  "clean_urls": true,
  "routes": {
    "/**": "index.html"
  }
}
```

2. Add `static.json` file to git

```bash
git add static.json
git commit -m "FEAT: Add static configuration for Heroku"
```

## Deploy to Heroku
Run the following commands from the terminal to create and finally deploy your Heroku application.

```bash
heroku login
heroku create

# IMPORTANT Don't skip this:
heroku buildpacks:add heroku/nodejs
heroku buildpacks:add https://github.com/heroku/heroku-buildpack-static

# Push your repo to the Heroku repository
git push heroku main # Depending on your setup, it might be master
```

### Source
The above information comes from the official [VueCLI documentation](https://cli.vuejs.org/guide/deployment.html#heroku)
