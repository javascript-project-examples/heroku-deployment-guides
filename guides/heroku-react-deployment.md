# React Deployment guide for Heroku 

Before you start, ensure you have installed the Heroku CLI.

> **NOTE:** This guide assumes you already have an React project up and running. 

#### Install [Heroku CLI](https://devcenter.heroku.com/articles/heroku-cli#download-and-install)

Follow the steps to deploy your React app to Heroku.

> **NB:** This must be done in the **root** of your project directory
> The same place you find the `package.json` file

#### 1. Create a `server.js` file

```javascript
const express = require('express');
const app = express();
const packageJson = require('./package.json')

// Middleware
app.use(requireHTTPS);
app.use(express.static('./build/' + packageJson.name));

// Redirect app request to build/index.html
app.get('/*', (req, res) => {
  res.sendFile('index.html', {root: 'build/' + packageJson.name});
});

// Start server
app.listen(process.env.PORT || 8080, () => console.log('Server started...'));

/**
 * @author: Klement Omeri
 * Special thanks to Klement for providing the function to redirect traffic from http to https
 */
function requireHTTPS(req, res, next) {
  // The 'x-forwarded-proto' check is for Heroku
  if (!req.secure && req.get('x-forwarded-proto') !== 'https') {
    return res.redirect('https://' + req.get('host') + req.url);
  }
  next();
}
```

#### 2. Install Express 
```bash
npm install express
```

#### 3. Update your `package.json`
The `start` script in your `package.json` should start your express server. Change it to run
`node server.js`. Also add `dev` to the list of scripts that runs `react-scripts start`.

```json
  ...
  "scripts": {
    "start": "node server.js",
    "dev": "react-scripts start",
    "build": "react-scripts build",
    "test": "react-scripts test",
    "eject": "react-scripts eject"
  },
  ...
```

> **NB!** Instead of using `npm start` you MUST use `npm run dev` to run your project locally during development.

#### 4. Deploy to Heroku
```bash
heroku login # If you have not logged in before
heroku create --region=eu
git add .
git commit -m "DEPLOY: Heroku buildpack"
git push heroku master # Could also be main depending on your setup.
```

# Source:
Written by Noroff Accelerate.
